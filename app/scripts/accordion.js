console.log('Cargando Accordion...');
    const dataAccordion = [
        {
            "title": "¿Dónde se encuentra localizada?",
            "desc": "450 oeste de los tribunales de justicia, Alajuela, Costa Rica."
        },
        {
            "title": " ¿Cuál es la duración de las carreras?",
            "desc": "Todas las carreras tienen una duración de dos años y ocho meses llevando el bloque completo."
        },
        {
            "title": "¿Cuál es el horario administrativo de oficinas?",
            "desc": "De lunes a viernes de 8 am a 6 pm. Cerrado los sábados (excepto el último sábado del mes)"
        },
    ];

    (function () {
        let ACCORDION = {
            init: function () {
                let _self = this
              
                this.insertData(_self);
                this.eventHandler(_self);
            },

            insertData: function (_self) {
                dataAccordion.map(function (item, index) {
                    document.querySelector('.main-accordion-container').insertAdjacentHTML('beforeend', _self.tplAccordionItem(item));
                });
            },

            eventHandler: function (_self) {
                let arrayRefs = document.querySelectorAll('.accordion-title');
                for (let x = 0; x < arrayRefs.length; x++) {
                    arrayRefs[x].addEventListener('click', function (event) {
                        console.log('event', event);
                        _self.showTab(event.target);
                    });    
                }
            },

            tplAccordionItem: function (item) {
                return (`<div class='accordion-item'>
                <div class='accordion-title'><p>${item.title}</p></div>
                <div class='accordion-desc'><p>${item.desc}</p></div>
                </div>`)
            },

            showTab: function (refItem) {
                let activeTab = document.querySelector('.tab-active')
                if (activeTab) {
                    activeTab.classList.remove('tab-active');
                }
                console.log('show tab', refItem);
                refItem.parentElement.classList.toggle('tab-active');
            },

        }
        ACCORDION.init();
    })();
