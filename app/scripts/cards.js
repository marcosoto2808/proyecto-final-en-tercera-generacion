console.log('Cargando Cards...');
const dataCards = [
    {
        "title": "Ingenieria industrial",
        "url_image": "https://ucem.ac.cr/wp-content/uploads/2021/11/WhatsApp-Image-2021-11-08-at-7.58.30-PM-2.jpeg",
        "desc": "Ingeniería industrial busca formas más prácticas y eficaces en lo referente a la producción de una empresa. Orienta de manera efectiva tanto al personal como a la maquinaria.",
        "cta": "Mostrar más...",
        "link": "https://ucem.ac.cr/wp-content/uploads/2022/07/IIIC-22-INDUSTRIAL.pdf"
    },
    {
        "title": "Ingenieria en Sistemas",
        "url_image": "https://ucem.ac.cr/wp-content/uploads/2021/11/WhatsApp-Image-2021-11-08-at-7.58.30-PM-3.jpeg",
        "desc": "Un ingeniero en sistemas labora en los campos del diseño y la programación de sistemas operativos en las empresas, así como el mantenimiento de estos y otros softwares utilizados en la institucion.",
        "cta": "Mostrar más...",
        "link": "https://ucem.ac.cr/wp-content/uploads/2022/07/IIIC-22-SISTEMAS.pdf"
    },
    {
        "title": "Administracion de Negocios",
        "url_image": "https://ucem.ac.cr/wp-content/uploads/2021/11/WhatsApp-Image-2021-11-08-at-7.58.30-PM.jpeg",
        "desc": "La Administración de Negocios es la disciplina encargada de la planificación, dirección y control de los recursos de una organización, con el fin de obtener su máximo rendimiento.",
        "cta": "Mostrar más...",
        "link": "https://ucem.ac.cr/wp-content/uploads/2022/07/IIIC-22-ADMINISTRACION.pdf"
    },
    {
        "title": "Contaduria",
        "url_image": "https://ucem.ac.cr/wp-content/uploads/2021/11/WhatsApp-Image-2021-11-08-at-7.58.30-PM-1.jpeg",
        "desc": "La Contaduría forma profesionales capaces de manejar estados financieros con el fin de facilitar la interpretación de los datos financieros, para la toma de decisiones en el ámbito socioeconómico.",
        "cta": "Mostrar más...",
        "link": "https://ucem.ac.cr/wp-content/uploads/2022/07/IIIC-22-CONTADURIA.pdf"
    },

];

(function () {
    let CARD = {
        init: function () {
            console.log('El módulo de carga funciona correctamente');
            let _self = this;
            
            this.insertData(_self);
        },

        insertData: function (_self) {
            dataCards.map(function (item, index) {
                document.querySelector('.card-list').insertAdjacentHTML('beforeend', _self.tplCardItem(item, index));
            });
        },

        tplCardItem: function (item, index) {
            return(`<div class='card-item' id="card-number-${index}">
            <img src="${item.url_image}"/>
            <div class='card-info'>
                <p class='card-title'>${item.title}</p>
                <p class='card-desc'>${item.desc}</p>
                <a class='card-cta' target='blank' href="${item.link}">${item.cta}</a>
            </div>
            </div>`)
        },
    }
    CARD.init();
})();

